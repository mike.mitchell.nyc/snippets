let csv = require("fast-csv"),
    fs = require('fs'),
    obj = {};

csv
    .fromPath("./usps-codes.csv")
    .on("data", function(data){
        if (!(data[0] in obj)) {
            obj[data[0]] = [data[1]];
        } else {
            obj[data[0]].push(data[1])
        }
    })
    .on("end", function(){
        let json = JSON.stringify(obj);
        fs.writeFile('myjsonfile.json', json, 'utf8', function (err) {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    });