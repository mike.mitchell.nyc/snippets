const fs = require("fs");
const _ = require("lodash");
let speciesList = require("../data/SpeciesList.json");
let tell = [];
fs.readdir("../data", "utf8", (err, directoryContents) => {
  let holdMyObjects = []
  if (err) throw err;
  const joinById = (
    idKeyName,
    parentCollection,
    childCollection,
    childMembersPropName
  ) =>
    parentCollection.map(record => ({
      ...record,
      [childMembersPropName]: childCollection.filter(
        childRecord => childRecord[idKeyName] === record[idKeyName]
      )
    }));
  let files = directoryContents
    .map(fileName => (fileName.match("json") ? fileName : ""))
    .filter(String);

  speciesList.forEach(treeObject => {
    let speciesId = treeObject.SpeciesID;
    holdMyObjects.push(treeObject);
    if (speciesId < 9) {
      files.forEach(file => {
        let json = require("../data/" + file);
        let fileNameWithoutSpacesOrJsonExtension = file
          .slice(0, -5)
          .replace(/ /g, "")
          .trim();

        holdMyObjects.push(
          joinById(
            "SpeciesID",
            holdMyObjects,
            json,
            fileNameWithoutSpacesOrJsonExtension
          )
        );
      });
      
    }
  
    tell.push(JSON.stringify(holdMyObjects))
  });
  console.log(tell);
});



const parent = [
  {
    speciesId: "a"
  },
  {
    speciesId: "b"
  },
  {
    speciesId: "c"
  },
  {
    speciesId: "d"
  }
];

const child1 = [
  {
    speciesId: "a",
    otherId: "a"
  },
  {
    speciesId: "a",
    otherId: "a"
  },
  {
    speciesId: "a",
    otherId: "a"
  },
  {
    speciesId: "b",
    otherId: "b"
  },
  {
    speciesId: "c",
    otherId: "c"
  },
  {
    speciesId: "d",
    otherId: "d"
  }
];

const child2 = [
  {
    otherId: "a"
  },
  {
    otherId: "b"
  },
  {
    otherId: "c"
  },
  {
    otherId: "d"
  }
];
