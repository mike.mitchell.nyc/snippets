const parent = [
    {
        speciesId: 'a'
    },
    {
        speciesId: 'b'
    },
    {
        speciesId: 'c'
    },
    {
        speciesId: 'd'
    }
];

const child1 = [
    {
        speciesId: 'a',
        otherId: 'a'
    },
    {
        speciesId: 'a',
        otherId: 'a'
    },
    {
        speciesId: 'a',
        otherId: 'a'
    },
    {
        speciesId: 'b',
        otherId: 'b'
    },
    {
        speciesId: 'c',
        otherId: 'c'
    },
    {
        speciesId: 'd',
        otherId: 'd'
    }
];

const child2 = [
    {
        otherId: 'a'
    },
    {
        otherId: 'b'
    },
    {
        otherId: 'c'
    },
    {
        otherId: 'd'
    }
];

const joinById = (
    idKeyName,
    parentCollection,
    childCollection,
    childMembersPropName
) =>
    parentCollection.map(record => ({
        ...record,
        [childMembersPropName]: childCollection.filter(
            childRecord => childRecord[idKeyName] === record[idKeyName]
        )
    }));

const child1xchild2 = joinById(
    'otherId',
    child1,
    child2,
    'child2MemberCollection'
);

const parentxchild1 = joinById(
    'speciesId',
    parent,
    child1xchild2,
    'child1MemberCollection'
);

console.log(JSON.stringify(parentxchild1,undefined,4))